<div align="center">

![](/assets/loki_full_small.png "loki")

<br>
<img src="https://img.shields.io/badge/go-%2300ADD8.svg?style=for-the-badge&logo=go&logoColor=white">
<img src="https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white">
<br><br>
<a href="#"><img src="https://img.shields.io/badge/build-passing-green"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
<br><br>
</div>

#### Prerequisites
- [Go](https://golang.org/)
- [Mysql](https://www.mysql.com/)

#### Configure database
> To configure database, you must execute the file ``` sql/init.sql ```

#### Steps to clone project and start loki webservice
```shell
git clone git@gitlab.com:rodrigoodhin/loki.git
cd loki
go run main.go
```

## Documentation
- [Gitlab repository](https://gitlab.com/rodrigoodhin/loki-doc)
- [Live project](https://rodrigoodhin.gitlab.io/loki-doc/)
